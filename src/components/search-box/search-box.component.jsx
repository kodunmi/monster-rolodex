import React from 'react'
import './search-box.style.css';
export const SearchBox = ({changeText}) => {    
    return(
        <div >
            <input type="text" className='search' onInput={changeText} placeholder="enter a monster"/>
        </div>
    )
}