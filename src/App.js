import React, { Component } from 'react';
import { css } from "@emotion/core";
import HashLoader from "react-spinners/HashLoader";
import './App.css';
import { CardList } from './components/card-list/card-list.component';
import { SearchBox } from './components/search-box/search-box.component';



const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;


class App extends Component {
  constructor() {
    super();

    this.state = {
      monsters: [],
      result: '',
      loading:true
    };
  };

  handleChange = e => this.setState({result: e.target.value})

  
  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(users => this.setState({ monsters: users, loading: false }))
  };
  render() {
    const {monsters , result} = this.state;
    const filteredMonster = monsters.filter(monster => monster.name.toLowerCase().includes(result.toLowerCase()));
      console.log(filteredMonster)
    return (

      <div className="App">
        <h1 className="header">Monster Rolladex</h1>
        <SearchBox changeText={ this.handleChange } />
        <CardList monsters={filteredMonster} />
        <HashLoader
          css={override}
          size={150}
          color={"#95dada"}
          loading={this.state.loading}
        />
      </div>
    );
  }
}


export default App;
